#!/bin/bash
#SBATCH --constraint knl
#SBATCH --qos regular
#SBATCH --job-name hpo-cpu
#SBATCH --time 180
#SBATCH --nodes 50
#SBATCH --ntasks-per-node 10
#SBATCH --cpus-per-task 1
#SBATCH --output %x-%j.out
#SBATCH --error %x-%j.err
module load parallel
module load pytorch/1.7.1
mkdir -p logs/checkpoints
python $HOME/hpo_uq/bin/hpo.py sampling /global/project/projectdirs/m3769/vincent/TEMP/salib/config.yaml
parallel --delay .2 -j 50 --joblog logs/runtask.log " srun --exclusive --nodes 1 --ntasks 10 --cpus-per-task 1 python $HOME/hpo_uq/bin/hpo.py evaluation /global/project/projectdirs/m3769/vincent/TEMP/salib/config.yaml && echo step {1}" ::: {0..49}
