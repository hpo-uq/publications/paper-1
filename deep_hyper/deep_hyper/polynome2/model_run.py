import numpy as np
import keras.backend as K
import keras
from keras.callbacks import EarlyStopping
from keras.layers import Dense, Dropout
from keras.models import Sequential
from tensorflow.keras.optimizers import RMSprop

import os
import sys

from deep_hyper.polynome2.load_data import load_data

def r2(y_true, y_pred):
    SS_res = keras.backend.sum(keras.backend.square(y_true - y_pred), axis=0)
    SS_tot = keras.backend.sum(
        keras.backend.square(y_true - keras.backend.mean(y_true, axis=0)), axis=0
    )
    output_scores = 1 - SS_res / (SS_tot + keras.backend.epsilon())
    r2 = keras.backend.mean(output_scores)
    return r2


HISTORY = None


def run(config):
    global HISTORY
    (x_train, y_train), (x_valid, y_valid) = load_data()
 
    config['dropout'] *= 0.1
    config['lr'] *= 0.001
   
    model = Sequential()
    
    for ii in range(config['layers']):
        if ii == 0:
            model.add(
                Dense(
                    units = config['units'],
                    activation = 'relu',
                    input_shape = tuple(np.shape(x_train)[1:])
                )
            )
        else:
            model.add(
                Dense(
                    units = config['units'],
                    activation = 'relu',
                )
            )
        if config['dropout']!=0:
            model.add(
                keras.layers.Dropout(
                    rate = config['dropout'],
                )
            )
    
    model.add(Dense(1))

    model.summary()

    model.compile(loss='mse', optimizer=RMSprop(learning_rate=config['lr']), metrics=[r2])

    history = model.fit(x_train, y_train,
                        batch_size=config['batch'],
                        epochs=config['epochs'],
                        verbose=0,
#                         callbacks=[EarlyStopping(
#                             monitor='val_r2',
#                             mode='max',
#                             verbose=1,
#                             patience=10
#                         )],
                        validation_data=(x_valid, y_valid))

    HISTORY = history.history

    return np.nan_to_num(history.history['val_r2'][-1])

def hyppo_run(**kwargs):
    res = run(kwargs)
    return -res

if __name__ == '__main__':
    config = {
        'units': 10,
        'layers': 2,
        'dropout': 0.1,
        'lr': 0.01,
        'epochs':10,
        'batch':64,
    }
    objective = run(config)
    print('objective: ', objective)
    import matplotlib.pyplot as plt
    plt.plot(HISTORY['val_r2'])
    plt.xlabel('Epochs')
    plt.ylabel('Objective: $R^2$')
    plt.grid()
    plt.show()
