from deephyper.problem import HpProblem

Problem = HpProblem()

Problem.add_dim('units', (1, 100))
Problem.add_dim('layers', (1,20))
Problem.add_dim('dropout', (0,10)) # x0.1
Problem.add_dim('lr', (1,100)) # x0.001
Problem.add_dim('epochs', (5,100))
Problem.add_dim('batch', (8,512))

Problem.add_starting_point(
    units=10,
    layers=1,
    dropout=0,
    lr=10,
    epochs=10,
    batch=64,
)

if __name__ == '__main__':
    print(Problem)
